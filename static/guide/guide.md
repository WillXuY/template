# I - ⚖️ License

Copyright (C) 2021  Weiyang(Will) Xu

This file is part of template.
Template is free software: you can redistribute it and/or modify
it under the term of the GNU General Public License version 3 or
any later version, as specified in the readme.md file.

# II. [Kubernetes Guide Notes](kubernetes/kubernetes.md)

Notes for [Kubernetes](https://kubernetes.io/) (include [Minikube](https://minikube.sigs.k8s.io/docs/))

# III. 
