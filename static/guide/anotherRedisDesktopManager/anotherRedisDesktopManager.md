# I - ⚖️ License

Copyright (C) 2021  Weiyang(Will) Xu

This file is part of template.
Template is free software: you can redistribute it and/or modify
it under the term of the GNU General Public License version 3 or
any later version, as specified in the readme.md file.

# [AnotherRedisDesktopManager](https://github.com/qishibo/AnotherRedisDesktopManager)

Redis connection tools which is easy to use.
