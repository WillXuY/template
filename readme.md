# I - ⚖️ License

Copyright (C) 2021  Weiyang(Will) Xu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# II - 📖 What's this?

This project is a demo microservice project for developing with spring boot, gradle and so on.
Take some notes for programing skill, tools using guide and so on.

# III - 🧭 Navigation 

## i. Include Another Flat Project: [algorithm](https://github.com/WillXuY/algorithm)

## ii. [Programing Notes](static/notes/notes.md) mark solutions meet in programing.

## iii. [Tools Using Guide](static/guide/guide.md) notes how to build project, use tools and so on. 

## iv. [Tools Using Example](src/test/java/org/willxu/template/tools/package-info.java)

# IV - 📚 Full Technology Stack

- language
  - java(main language)
  - markdown(take notes)
  - groovy(build tool language)
  
- build tool
  - gradle(deal with the dependencies)
  
- framework
  - spring boot(main framework)
  - spring cloud(microservice)
  - mybatis(databases)
  
- databases
  - mysql(main databases)
  
- nosql
  - redis(cache)
  
- tools
  - lombok(create the POJO)
  - jackson(deal with the json)
  