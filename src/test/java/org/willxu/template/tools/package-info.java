/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of template.
 * Template is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

/**
 * It's a package include utils using examples.
 * <br/><br/>
 * Includes:
 * <br/>
 * I. Jackson Guild
 * {@link org.willxu.template.tools.JacksonTest}
 * <br/>
 * II.
 */
package org.willxu.template.tools;
