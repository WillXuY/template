/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of template.
 * Template is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.template.build;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.willxu.algorithm.provide.TreeNode;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class GradleIncludeOtherProjectTest {
    @Test
    public void testIncludeAlgorithmDomain() {
        TreeNode treeNode = new TreeNode(0);
        assertEquals(0, treeNode.val);
    }
}
