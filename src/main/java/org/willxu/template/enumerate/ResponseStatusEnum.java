/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of template.
 * Template is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.template.enumerate;

public enum ResponseStatusEnum {
    SUCCESS("success"),
    FAIL("fail"),
    EXCEPTION("exception");

    private final String STATUS;

    ResponseStatusEnum(String status) {
        this.STATUS = status;
    }

    public String getStatus() {
        return STATUS;
    }
}
